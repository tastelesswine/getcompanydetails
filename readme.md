# getComapnyDetails
This Project is a **ETL** based project which fetch data for **Companies House** in form various api endpoints or bulk download method, and then apply various filter on the data.

**

## ***#Techstack***

** Python, Spark, MognoDB, Jupyter Notebook and other python libraries.
 

# Folder and Files
    #**JupyterNotebook**(folder): This is root directory of the folder.
	#**data**(folder): In this folder all the companies data in the csv format with various filter.
    #**extractingdata.ipynp** and **testing_the_defence.ipynb** these Jupyter notebooks are for mainly for data exploration and understanding.
	#**directorAgeCheck.ipynb**  this jupyter notebook for is the **ETL** process for creating list of companies whose mean age of director age is greater than 55 years.
	#**dirAgeVis.ipynb** this jupyter notebook visualize the list of companies whose mean age of director age is greater than 55 years and convert into csv.
	#**revenueCheck.ipynb** this jupyter notebook for is the **ETL** process for creating list of companies whose revenue is between 5 to 20 million pound.
	#****RevnueWithDiRAgeVis.ipynb****  his jupyter notebook visualize the list of companies whose revenue is between 5 to 20 million pound and convert into csv.
	#****ComapnyDataSouthEngwithBasicCol.csv**** this  csv file which has list of companies in south of england.
	#****ComapnyDataSouthEngwithBasicColDirAge.csv**** this csv file contain list of companies whose active director mean age is greater than 55 years.
	#****ComapnyDataSouthEngwithBasicColDirAgeWithRev.csv**** this csv file contain list of companies whose revenue is between 5 to 20 million pound.

# How to Run!
	#1 Firstly Install the various python packages.
	#2 Run extractingdata.ipynb, testing_the_defence.ipynb to get data insight.
	#3 Run directorAgeCheck.ipynb, this will start the 1st ETL Process.
	#4 Run dirAgeVis.ipynb, this will save the output of 1st ETL Process into csv file.
	#5 Run revenueCheck.ipynb, this will start the 2nd ETL Process.
	#6 Run RevnueWithDiRAgeVis.ipynb, this will save the output of 2nd ETL Process into csv file.